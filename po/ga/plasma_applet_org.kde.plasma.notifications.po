# Irish translation of plasma_applet_notifications
# Copyright (C) 2011 This_file_is_part_of_KDE
# This file is distributed under the same license as the plasma_applet_notifications package.
# Kevin Scannell <kscanne@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_notifications\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2011-12-28 12:28-0500\n"
"Last-Translator: Kevin Scannell <kscanne@gmail.com>\n"
"Language-Team: Irish <gaeilge-gnulinux@lists.sourceforge.net>\n"
"Language: ga\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=5; plural=n==1 ? 0 : n==2 ? 1 : n<7 ? 2 : n < 11 ? "
"3 : 4\n"

#: fileinfo.cpp:170
#, kde-format
msgid "Open with %1"
msgstr ""

#: fileinfo.cpp:174
#, kde-format
msgid "Open with…"
msgstr ""

#: filemenu.cpp:117 package/contents/ui/JobItem.qml:268
#, kde-format
msgid "Open Containing Folder"
msgstr ""

#: filemenu.cpp:131
#, fuzzy, kde-format
#| msgid "Copy"
msgid "&Copy"
msgstr "Cóipeáil"

#: filemenu.cpp:139
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr ""

#: filemenu.cpp:195
#, kde-format
msgid "Properties"
msgstr ""

#: globalshortcuts.cpp:23
#, kde-format
msgid "Toggle do not disturb"
msgstr ""

#: globalshortcuts.cpp:42
#, fuzzy, kde-format
#| msgid "Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications Off"
msgstr "Fógairt"

#: globalshortcuts.cpp:43
#, fuzzy, kde-format
#| msgid "Notifications"
msgctxt "OSD popup, keep short"
msgid "Notifications On"
msgstr "Fógairt"

#: package/contents/ui/EditContextMenu.qml:32
#, fuzzy, kde-format
#| msgid "Copy"
msgctxt "@action:inmenu"
msgid "Copy Link Address"
msgstr "Cóipeáil"

#: package/contents/ui/EditContextMenu.qml:44
#, fuzzy, kde-format
#| msgid "Copy"
msgctxt "@action:inmenu"
msgid "Copy"
msgstr "Cóipeáil"

#: package/contents/ui/EditContextMenu.qml:61
#, fuzzy, kde-format
#| msgid "Select All"
msgctxt "@action:inmenu"
msgid "Select All"
msgstr "Roghnaigh Uile"

#: package/contents/ui/FullRepresentation.qml:69
#, kde-format
msgid "Do not disturb"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:111
#, kde-format
msgid "For 1 hour"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:116
#, kde-format
msgid "For 4 hours"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:125
#, kde-format
msgid "Until this evening"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:135
#, kde-format
msgid "Until tomorrow morning"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:147
#, kde-format
msgid "Until Monday"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:154
#, kde-format
msgid "Until manually disabled"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:204
#, fuzzy, kde-format
#| msgid "Automatically hide"
msgctxt "Do not disturb until date"
msgid "Automatically ends: %1"
msgstr "Folaigh go huathoibríoch"

#: package/contents/ui/FullRepresentation.qml:216
#, kde-format
msgctxt "Do not disturb until app has finished (reason)"
msgid "While %1 is active (%2)"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:218
#, kde-format
msgctxt "Do not disturb until app has finished"
msgid "While %1 is active"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:224
#, kde-format
msgctxt "Do not disturb because external mirrored screens connected"
msgid "Screens are mirrored"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:432
#, kde-format
msgid "Close Group"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:577
#, kde-format
msgid "Show Fewer"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:579
#, kde-format
msgctxt "Expand to show n more notifications"
msgid "Show %1 More"
msgstr ""

#: package/contents/ui/FullRepresentation.qml:620
#: package/contents/ui/main.qml:105
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "No unread notifications"
msgstr "Fógairt"

#: package/contents/ui/FullRepresentation.qml:635
#: package/contents/ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Notifications and Jobs"
msgid "Notification service not available"
msgstr "Fógraí agus Jabanna"

#: package/contents/ui/FullRepresentation.qml:637
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2'"
msgstr ""

#: package/contents/ui/JobDetails.qml:34
#, fuzzy, kde-format
#| msgid "%1:"
msgctxt "Row description, e.g. Source"
msgid "%1:"
msgstr "%1:"

#: package/contents/ui/JobDetails.qml:105
#, fuzzy, kde-format
#| msgctxt ""
#| "How much many bytes (or whether unit in the locale has been copied over "
#| "total"
#| msgid "%1 of %2"
msgctxt "How many bytes have been copied"
msgid "%2 of %1"
msgstr "%1 as %2"

#: package/contents/ui/JobDetails.qml:109
#, fuzzy, kde-format
#| msgid "%2 / 1 file"
#| msgid_plural "%2 / %1 files"
msgctxt "How many files have been copied"
msgid "%2 of %1 file"
msgid_plural "%2 of %1 files"
msgstr[0] "%2 / 1 chomhad"
msgstr[1] "%2 / %1 chomhad"
msgstr[2] "%2 / %1 chomhad"
msgstr[3] "%2 / %1 gcomhad"
msgstr[4] "%2 / %1 comhad"

#: package/contents/ui/JobDetails.qml:112
#, fuzzy, kde-format
#| msgid "%2 / 1 folder"
#| msgid_plural "%2 / %1 folders"
msgctxt "How many dirs have been copied"
msgid "%2 of %1 folder"
msgid_plural "%2 of %1 folders"
msgstr[0] "%2 / 1 fhillteán"
msgstr[1] "%2 / %1 fhillteán"
msgstr[2] "%2 / %1 fhillteán"
msgstr[3] "%2 / %1 bhfillteán"
msgstr[4] "%2 / %1 fillteán"

#: package/contents/ui/JobDetails.qml:115
#, fuzzy, kde-format
#| msgid "%2 / 1 file"
#| msgid_plural "%2 / %1 files"
msgctxt "How many items (that includes files and dirs) have been copied"
msgid "%2 of %1 item"
msgid_plural "%2 of %1 items"
msgstr[0] "%2 / 1 chomhad"
msgstr[1] "%2 / %1 chomhad"
msgstr[2] "%2 / %1 chomhad"
msgstr[3] "%2 / %1 gcomhad"
msgstr[4] "%2 / %1 comhad"

#: package/contents/ui/JobDetails.qml:123
#, fuzzy, kde-format
#| msgid "%2 / 1 file"
#| msgid_plural "%2 / %1 files"
msgid "%1 file"
msgid_plural "%1 files"
msgstr[0] "%2 / 1 chomhad"
msgstr[1] "%2 / %1 chomhad"
msgstr[2] "%2 / %1 chomhad"
msgstr[3] "%2 / %1 gcomhad"
msgstr[4] "%2 / %1 comhad"

#: package/contents/ui/JobDetails.qml:125
#, fuzzy, kde-format
#| msgid "%2 / 1 folder"
#| msgid_plural "%2 / %1 folders"
msgid "%1 folder"
msgid_plural "%1 folders"
msgstr[0] "%2 / 1 fhillteán"
msgstr[1] "%2 / %1 fhillteán"
msgstr[2] "%2 / %1 fhillteán"
msgstr[3] "%2 / %1 bhfillteán"
msgstr[4] "%2 / %1 fillteán"

#: package/contents/ui/JobDetails.qml:127
#, kde-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: package/contents/ui/JobDetails.qml:144
#, kde-format
msgctxt "Bytes per second"
msgid "%1/s"
msgstr ""

#: package/contents/ui/JobItem.qml:164
#, fuzzy, kde-format
#| msgid "%1:"
msgctxt "Percentage of a job"
msgid "%1%"
msgstr "%1:"

#: package/contents/ui/JobItem.qml:178
#, fuzzy, kde-format
#| msgid "Paused"
msgctxt "Pause running job"
msgid "Pause"
msgstr "Moillithe"

#: package/contents/ui/JobItem.qml:188
#, fuzzy, kde-format
#| msgid "Cancel job"
msgctxt "Cancel running job"
msgid "Cancel"
msgstr "Cealaigh an jab"

#: package/contents/ui/JobItem.qml:199
#, kde-format
msgctxt "A button tooltip; hides item details"
msgid "Hide Details"
msgstr ""

#: package/contents/ui/JobItem.qml:200
#, kde-format
msgctxt "A button tooltip; expands the item to show details"
msgid "Show Details"
msgstr ""

#: package/contents/ui/JobItem.qml:232
#: package/contents/ui/ThumbnailStrip.qml:169
#, kde-format
msgid "More Options…"
msgstr ""

#: package/contents/ui/JobItem.qml:260
#, fuzzy, kde-format
#| msgid "Open"
msgid "Open"
msgstr "Oscail"

#: package/contents/ui/main.qml:63
#, kde-format
msgid "%1 running job"
msgid_plural "%1 running jobs"
msgstr[0] "%1 jab ar siúl"
msgstr[1] "%1 jab ar siúl"
msgstr[2] "%1 jab ar siúl"
msgstr[3] "%1 jab ar siúl"
msgstr[4] "%1 jab ar siúl"

#: package/contents/ui/main.qml:67
#, kde-format
msgctxt "Job title (percentage)"
msgid "%1 (%2%)"
msgstr ""

#: package/contents/ui/main.qml:72
#, fuzzy, kde-format
#| msgctxt "Number of jobs and the speed at which they are downloading"
#| msgid "%1 running job (%2/s)"
#| msgid_plural "%1 running jobs (%2/s)"
msgid "%1 running job (%2%)"
msgid_plural "%1 running jobs (%2%)"
msgstr[0] "%1 jab ar siúl (%2/s)"
msgstr[1] "%1 jab ar siúl (%2/s)"
msgstr[2] "%1 jab ar siúl (%2/s)"
msgstr[3] "%1 jab ar siúl (%2/s)"
msgstr[4] "%1 jab ar siúl (%2/s)"

#: package/contents/ui/main.qml:85
#, fuzzy, kde-format
#| msgid "%1 notification"
#| msgid_plural "%1 notifications"
msgid "%1 unread notification"
msgid_plural "%1 unread notifications"
msgstr[0] "%1 fhógra"
msgstr[1] "%1 fhógra"
msgstr[2] "%1 fhógra"
msgstr[3] "%1 bhfógra"
msgstr[4] "%1 fógra"

#: package/contents/ui/main.qml:98
#, kde-format
msgid "Do not disturb until %1; middle-click to exit now"
msgstr ""

#: package/contents/ui/main.qml:101
#, kde-format
msgid "Do not disturb mode active; middle-click to exit"
msgstr ""

#: package/contents/ui/main.qml:107
#, kde-format
msgid "Middle-click to enter do not disturb mode"
msgstr ""

#: package/contents/ui/main.qml:229
#, fuzzy, kde-format
#| msgid "Notifications"
msgid "Clear All Notifications"
msgstr "Fógairt"

#: package/contents/ui/main.qml:248
#, kde-format
msgid "&Configure Event Notifications and Actions…"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:120
#, kde-format
msgctxt "Notification was added less than a minute ago, keep short"
msgid "Just now"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:126
#, kde-format
msgctxt "Notification was added minutes ago, keep short"
msgid "%1 min ago"
msgid_plural "%1 min ago"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""
msgstr[4] ""

#: package/contents/ui/NotificationHeader.qml:160
#, fuzzy, kde-format
#| msgctxt "Speed and estimated time to completition"
#| msgid "%1 (%2 remaining)"
msgctxt "seconds remaining, keep short"
msgid "%1 s remaining"
msgid_plural "%1 s remaining"
msgstr[0] "%1 (%2 fágtha)"
msgstr[1] "%1 (%2 fágtha)"
msgstr[2] "%1 (%2 fágtha)"
msgstr[3] "%1 (%2 fágtha)"
msgstr[4] "%1 (%2 fágtha)"

#: package/contents/ui/NotificationHeader.qml:164
#, fuzzy, kde-format
#| msgctxt "Speed and estimated time to completition"
#| msgid "%1 (%2 remaining)"
msgctxt "minutes remaining, keep short"
msgid "%1 min remaining"
msgid_plural "%1 min remaining"
msgstr[0] "%1 (%2 fágtha)"
msgstr[1] "%1 (%2 fágtha)"
msgstr[2] "%1 (%2 fágtha)"
msgstr[3] "%1 (%2 fágtha)"
msgstr[4] "%1 (%2 fágtha)"

#: package/contents/ui/NotificationHeader.qml:169
#, fuzzy, kde-format
#| msgctxt "Speed and estimated time to completition"
#| msgid "%1 (%2 remaining)"
msgctxt "hours remaining, keep short"
msgid "%1 h remaining"
msgid_plural "%1 h remaining"
msgstr[0] "%1 (%2 fágtha)"
msgstr[1] "%1 (%2 fágtha)"
msgstr[2] "%1 (%2 fágtha)"
msgstr[3] "%1 (%2 fágtha)"
msgstr[4] "%1 (%2 fágtha)"

#: package/contents/ui/NotificationHeader.qml:189
#, kde-format
msgid "Configure"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:206
#, kde-format
msgctxt "Opposite of minimize"
msgid "Restore"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:207
#, kde-format
msgid "Minimize"
msgstr ""

#: package/contents/ui/NotificationHeader.qml:230
#, kde-format
msgid "Close"
msgstr ""

#: package/contents/ui/NotificationItem.qml:186
#, fuzzy, kde-format
#| msgctxt ""
#| "%1 is the name of the job, can be things like Copying, deleting, moving"
#| msgid "%1 [Paused]"
msgctxt "Job name, e.g. Copying is paused"
msgid "%1 (Paused)"
msgstr "%1 [Ar Shos]"

#: package/contents/ui/NotificationItem.qml:191
#, fuzzy, kde-format
#| msgid "%1 [Finished]"
msgctxt "Job name, e.g. Copying has failed"
msgid "%1 (Failed)"
msgstr "%1 [Críochnaithe]"

#: package/contents/ui/NotificationItem.qml:193
#, fuzzy, kde-format
#| msgid "%1 [Finished]"
msgid "Job Failed"
msgstr "%1 [Críochnaithe]"

#: package/contents/ui/NotificationItem.qml:197
#, fuzzy, kde-format
#| msgid "%1 [Finished]"
msgctxt "Job name, e.g. Copying has finished"
msgid "%1 (Finished)"
msgstr "%1 [Críochnaithe]"

#: package/contents/ui/NotificationItem.qml:199
#, fuzzy, kde-format
#| msgid "%1 [Finished]"
msgid "Job Finished"
msgstr "%1 [Críochnaithe]"

#: package/contents/ui/NotificationItem.qml:346
#, kde-format
msgctxt "Reply to message"
msgid "Reply"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:36
#, kde-format
msgctxt "Text field placeholder"
msgid "Type a reply…"
msgstr ""

#: package/contents/ui/NotificationReplyField.qml:56
#, kde-format
msgctxt "@action:button"
msgid "Send"
msgstr ""

#, fuzzy
#~| msgid "Select All"
#~ msgctxt "Clear all notifications"
#~ msgid "Clear All"
#~ msgstr "Roghnaigh Uile"

#, fuzzy
#~| msgid "Resume job"
#~ msgctxt "Resume paused job"
#~ msgid "Resume"
#~ msgstr "Atosaigh an jab"

#, fuzzy
#~| msgid "Notifications"
#~ msgid "No unread notifications."
#~ msgstr "Fógairt"

#~ msgid "Information"
#~ msgstr "Eolas"

#, fuzzy
#~| msgid "Application notifications"
#~ msgid "Show application and system notifications"
#~ msgstr "Fógraí feidhmchláir"

#, fuzzy
#~| msgid "No active jobs or notifications"
#~ msgid "Show a history of notifications"
#~ msgstr "Níl aon jabanna ná fógraí gníomhacha ann"

#, fuzzy
#~| msgid "File transfers and other jobs"
#~ msgid "Track file transfers and other jobs"
#~ msgstr "Aistrithe comhad agus jabanna eile"

#~ msgid "%1 of %2 %3"
#~ msgstr "%1 as %2 %3"

#~ msgid "No notifications or jobs"
#~ msgstr "Níl aon fhógraí ná jabanna ann"

#, fuzzy
#~| msgid "%1:"
#~ msgctxt ""
#~ "Indicator that there are more urls in the notification than previews shown"
#~ msgid "+%1"
#~ msgstr "%1:"

#, fuzzy
#~| msgid "Application notifications"
#~ msgid "&Application notifications:"
#~ msgstr "Fógraí feidhmchláir"

#, fuzzy
#~| msgid "File transfers and other jobs"
#~ msgid "&File transfers and jobs:"
#~ msgstr "Aistrithe comhad agus jabanna eile"

#~ msgid "Choose which information to show"
#~ msgstr "Roghnaigh an fhaisnéis le taispeáint"

#~ msgid "Transfers"
#~ msgstr "Aistrithe"

#~ msgid "All"
#~ msgstr "Uile"

#~ msgid "KiB/s"
#~ msgstr "KiB/s"

#~ msgid "%1 file, to: %2"
#~ msgid_plural "%1 files, to: %2"
#~ msgstr[0] "%1 chomhad, go: %2"
#~ msgstr[1] "%1 chomhad, go: %2"
#~ msgstr[2] "%1 chomhad, go: %2"
#~ msgstr[3] "%1 gcomhad, go: %2"
#~ msgstr[4] "%1 comhad, go: %2"

#~ msgid "1 running job (%2 remaining)"
#~ msgid_plural "%1 running jobs (%2 remaining)"
#~ msgstr[0] "1 jab ag rith (%2 jab fágtha)"
#~ msgstr[1] "%1 jab ag rith (%2 jab fágtha)"
#~ msgstr[2] "%1 jab ag rith (%2 jab fágtha)"
#~ msgstr[3] "%1 jab ag rith (%2 jab fágtha)"
#~ msgstr[4] "%1 jab ag rith (%2 jab fágtha)"

#~ msgid "%1 suspended job"
#~ msgid_plural "%1 suspended jobs"
#~ msgstr[0] "%1 jab ar fionraí"
#~ msgstr[1] "%1 jab ar fionraí"
#~ msgstr[2] "%1 jab ar fionraí"
#~ msgstr[3] "%1 jab ar fionraí"
#~ msgstr[4] "%1 jab ar fionraí"

#~ msgid "%1 completed job"
#~ msgid_plural "%1 completed jobs"
#~ msgstr[0] "%1 jab críochnaithe"
#~ msgstr[1] "%1 jab críochnaithe"
#~ msgstr[2] "%1 jab críochnaithe"
#~ msgstr[3] "%1 jab críochnaithe"
#~ msgstr[4] "%1 jab críochnaithe"

#~ msgctxt "Generic title for the job transfer popup"
#~ msgid "Jobs"
#~ msgstr "Jabanna"

#~ msgid "More"
#~ msgstr "Tuilleadh"

#~ msgid "Pause job"
#~ msgstr "Cuir jab ar shos"

#~ msgctxt ""
#~ "%1 is the name of the job, can be things like Copying, deleting, moving"
#~ msgid "%1 [Finished]"
#~ msgstr "%1 [Críochnaithe]"

#~ msgid "Less"
#~ msgstr "Níos Lú"

#~ msgid "Notification from %1"
#~ msgstr "Fógra ó %1"

#~ msgid "Pop Up Notices"
#~ msgstr "Preabfhógraí"

#~ msgid "Popup"
#~ msgstr "Preabfhuinneog"
